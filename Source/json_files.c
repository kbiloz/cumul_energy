#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parson.h"
#include "json_files.h"


settings_t json_files_read(void)
{
    JSON_Value *root_value = NULL;
    JSON_Object *root_object = NULL;
    settings_t settings;

    root_value = json_parse_file("config.txt");
    root_object = json_value_get_object(root_value);

    strcpy(settings.dest, json_object_get_string(root_object, "destination"));
    strcpy(settings.port, json_object_dotget_string(root_object,
                                                    "settings.port"));
    strcpy(settings.parity, json_object_dotget_string(root_object,
                                                    "settings.parity"));
    settings.speed = json_object_dotget_number(root_object, "settings.speed");
    settings.slave_id = json_object_dotget_number(root_object,
                                                 "settings.slave_id");
    settings.data_bit = json_object_dotget_number(root_object,
                                                 "settings.data_bit");
    settings.stop_bit = json_object_dotget_number(root_object,
                                                "settings.stop_bit");
    settings.reg_addr = json_object_dotget_number(root_object,
                                                "settings.register_address");
    
    json_value_free(root_value);

    return settings;
}


void json_files_write(char dest[256], int32_t value, unsigned timestamp)
{
    FILE *f = NULL;
    JSON_Value *root_value = NULL;
    JSON_Object *root_object = NULL;
    char *serialized_string = NULL;

    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);

    json_object_set_string(root_object, "variable_name", "cumul_energy_kWh");
    json_object_set_number(root_object, "timestamp", timestamp);
    json_object_set_number(root_object, "value", value);

    serialized_string = json_serialize_to_string_pretty(root_value);
    f = fopen(dest, "w");

    if (f == NULL) {
        printf("Error opening file!\n");
        exit(1);
    }

    fprintf(f, "%s\n", serialized_string);
    fclose(f);

    printf("Output file saved to: %s\n", dest);

    json_free_serialized_string(serialized_string);
    json_value_free(root_value);
}
