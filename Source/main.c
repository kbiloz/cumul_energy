#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "json_files.h"
#include "modbus.h"


#define READ_REG_CNT    2


int main(void)
{
    settings_t settings;
    char *device_port = NULL;
    uint16_t reg[READ_REG_CNT] = {0};
    unsigned timestamp = 0;
    modbus_t *ctx = NULL;
    int32_t cumul_energy = 0;

    settings = json_files_read();
    device_port = settings.port;

    ctx = modbus_new_rtu(device_port, settings.speed, settings.parity[0],
                                        settings.data_bit, settings.stop_bit);
    if (ctx == NULL) {
        fprintf(stderr, "Unable to create the libmodbus context\n");
        return -1;
    }

    if (modbus_set_slave(ctx, settings.slave_id) == -1) {
        fprintf(stderr, "Setting slave failed: %s\n", modbus_strerror(errno));
        return -1;
    }

    if (modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485) == -1) {
        fprintf(stderr, "Serial mode setup fail: %s\n", modbus_strerror(errno));
        return -1;
    }

    if (modbus_connect(ctx) == -1) {
        fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    modbus_flush(ctx);

    memset(reg, 0, sizeof reg);

    if (modbus_read_registers(ctx, settings.reg_addr, READ_REG_CNT, reg) == -1) {
        fprintf(stderr, "Error while reading: %s\n", modbus_strerror(errno));
        return -1;
    }

    timestamp = time(NULL);
    cumul_energy = MODBUS_GET_INT32_FROM_INT16(reg, 0);
    json_files_write(settings.dest, cumul_energy, timestamp);

    modbus_free(ctx);

    return 0;
}
