#ifndef JSON_FILES_H
#define JSON_FILES_H

#include <stdint.h>


typedef struct {
    char port[256];
    char parity[2];
    char dest[256];
    int speed;
    int slave_id; 
    int data_bit;
    int stop_bit;
    int reg_addr;
} settings_t;

settings_t json_files_read(void);
void json_files_write(char dest[256], int32_t value, unsigned timestamp);

#endif /* JSON_FILES_H */
