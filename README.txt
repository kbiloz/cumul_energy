INSTALLATION

Software is using libmodbus library for MODBUS communication and parson library
for json support.

Clone libmodbus git repo: git https://github.com/stephane/libmodbus.git
Ensure, that you have automake, autoconf, libtool and a C compiler.
To install, just run, ./configure && make && sudo make install. Run ./autogen.sh
first to generate the configure script if required.
Ensure, that installation directory is /usr/local/, if not, then run 
./configure --prefix=/usr/local/ and installation again.

Parson library is provided with current git repo.

Configuration file is json-formatted and contains output file destination and
MODBUS settings, such as port, speed, slave id, parity, data bit, stop bit and
starting read register address.

Output file is json-formatted and contains variable name, timestamp and value.

RUN

To build and run source code, run: make basen
To run code, type: ./basen
