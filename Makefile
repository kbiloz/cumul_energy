CC = gcc
CFLAGS = -O0 -g -Wall -std=c99 -I/usr/local/include/modbus/ -L/usr/local/lib/

basen: Source/main.c Source/json_files.c
	$(CC) $(CFLAGS) Source/main.c Source/json_files.c Source/parson.c -o $@ -lmodbus
	./$@

clean:
	rm -f basen *.o
